def hashAoQuadrado(hash) 
    arrayAoQuadrado = []
    hash.each_key do |chave|
        arrayAoQuadrado.append(hash[chave]**2)
    end
    return arrayAoQuadrado
end

hash = {:chave1 => 5, :chave2 => 30, :chave3 => 20}
array = hashAoQuadrado(hash)

puts array