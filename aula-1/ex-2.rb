def operacoesArrays (listArray) 
    soma = []
    sub = []
    mult = []
    div = []
    listArray.each do |array|
        contador = 0
        somador       = array[0]
        subtrator     = array[0]
        multiplicador = array[0]
        divisor       = array[0] 
        array.each do |elemento|
            if contador > 0
                somador       += elemento
                subtrator     -= elemento
                multiplicador *= elemento
                divisor       /= elemento
            end
            contador++
        end
        soma.append(somador)
        sub.append(subtrator)
        mult.append(multiplicador)
        div.append(divisor)
    end
    puts "Soma: #{soma}"
    puts "Subtração: #{sub}"
    puts "Multiplicação: #{mult}"
    puts "Divisão: #{div}"
    # return soma, sub, mult, div
end

operacoesArrays([[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]])